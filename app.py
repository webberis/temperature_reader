from prometheus_client import start_http_server, Gauge

import random
import time

# Create a metric to track time spent and requests made.
TEMPERATURE = Gauge('temperature', 'Temperature')

def scan_temperature():
    temp = random.randint(10,30)
    TEMPERATURE.set(temp)
    print(temp)


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    # Generate some requests.
    while True:
        scan_temperature()
        time.sleep(5)