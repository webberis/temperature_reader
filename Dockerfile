FROM python:3 AS builder
#don't run dotnet restore
ADD app.py /

RUN pip install prometheus_client

FROM arm64v8/python:3-buster
COPY --from=builder app.py /
CMD [ "python", "./app.py" ]